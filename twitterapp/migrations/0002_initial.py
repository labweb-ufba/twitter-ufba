# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Usuario'
        db.create_table(u'twitterapp_usuario', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('nome_usuario', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('senha', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'twitterapp', ['Usuario'])

        # Adding model 'Tweet'
        db.create_table(u'twitterapp_tweet', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('texto', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('data', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('autor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['twitterapp.Usuario'])),
        ))
        db.send_create_signal(u'twitterapp', ['Tweet'])


    def backwards(self, orm):
        # Deleting model 'Usuario'
        db.delete_table(u'twitterapp_usuario')

        # Deleting model 'Tweet'
        db.delete_table(u'twitterapp_tweet')


    models = {
        u'twitterapp.tweet': {
            'Meta': {'object_name': 'Tweet'},
            'autor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['twitterapp.Usuario']"}),
            'data': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'texto': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        u'twitterapp.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nome_usuario': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'senha': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['twitterapp']