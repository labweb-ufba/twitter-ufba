# Sistema de Notícias do DCC - UFBA #

Projeto de MATC84 - Laboratório de Programaçao Web, executado pela equipe:

1. Bruno Moiteiro
2. Douglas Barbosa
3. José Olímpio
4. Nanci Bonfim
5. Raniere Santos
6. Vinícius Lins

### Parte 1 - Sistema web ###

## Dependências ##

* Python 2.7
* Django 1.6+
* SQlite
* South
* Django-Bootstrap3

**No repositório está disponível o arquivo "dependencias.txt", com os comandos de instalação dessas dependências.**

## Rodando ##
Após instalar as dependências é necessário rodar o syncdb apenas para a primeira vez.

```
#!python

python manage.py syncdb # Sincronizar o banco pela primeira vez apenas.
python manage.py schemamigration twitterapp --initial # Rodar pela primeira vez o South
```

O South usa migrações para refletir alterações dos modelos no banco de dados.
Cada vez que um modelo for alterado é necessário rodar:

```
#!python
python manage.py schemamigration twitterapp # Cria um esquema de migrações
python manage.py migrate twitterapp # Aplica as migrações

```

Roda o servidor
```
#!python

python manage.py runserver
```