URL_API = 'http://sistema-de-noticias-webservice.herokuapp.com'
URL_USERS = URL_API + '/users'
URL_TWEETS = URL_API + '/tweets'
URL_SESSION = URL_API + '/session'
HEADERS = {'content-type': 'application/json'}
