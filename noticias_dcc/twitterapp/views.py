import requests, slumber, json
from django import forms
from django.shortcuts import render, redirect
from django.views.generic import ListView, TemplateView, FormView
from django.views.generic.base import ContextMixin
from django.http import HttpResponseRedirect
from django.contrib import auth
from twitterapp import configurations as conf

# ----------------------------------------------------------------------------------------
# User form
# ----------------------------------------------------------------------------------------
class FormularioUsuario(forms.Form):
    nome = forms.CharField(label="Nome")
    nome_usuario = forms.CharField(label="Username")
    email = forms.EmailField()
    senha = forms.CharField(widget=forms.PasswordInput())


class CadastraUsuarios(FormView):
    template_name = "cadastra_usuarios.html"
    form_class = FormularioUsuario
    success_url = "/usuarios"

    def form_valid(self, form):
        novo = {
            'username': form.cleaned_data['nome_usuario'],
            'password': form.cleaned_data['senha'],
            'name': form.cleaned_data['nome'],
            'email': form.cleaned_data['email']
        }

        r = requests.post(conf.URL_USERS, data=json.dumps(novo), headers=conf.HEADERS)

        return redirect('CadastraUsuarios')

# ----------------------------------------------------------------------------------------
# Login
# ----------------------------------------------------------------------------------------
class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(label="Senha", widget=forms.PasswordInput)


class LoginView(FormView):
    template_name = "login.html"
    form_class = LoginForm
    success_url = "/usuarios"

    def form_valid(self, form):
        session = {
            'email': form.cleaned_data['email'],
            'password': form.cleaned_data['password']
        }

        r = requests.post(conf.URL_SESSION, data=json.dumps(session), headers=conf.HEADERS)
        print '*****'
        print r
        return HttpResponseRedirect(HomeView)

# ----------------------------------------------------------------------------------------
# Home View
# ----------------------------------------------------------------------------------------
class HomeView(TemplateView):
    template_name = "index.html"

# ----------------------------------------------------------------------------------------
# Tweet form
# ----------------------------------------------------------------------------------------
class FormTweet(forms.Form):
    texto = forms.CharField(label='Tweet',widget=forms.Textarea)


class EnviaTweet(FormView):
    template_name = "envia_tweet.html"
    form_class = FormTweet

# ----------------------------------------------------------------------------------------
# Show users
# ----------------------------------------------------------------------------------------
class ExibeUsuarios(TemplateView):

    template_name = "exibe_usuarios.html"

    def get_context_data(self, **kwargs):
        context = super(ExibeUsuarios, self).get_context_data(**kwargs)
        api = slumber.API(conf.URL_API)
        context.update({'usuarios': api.users.get()})

        return context

# ----------------------------------------------------------------------------------------
# Show tweets
# ----------------------------------------------------------------------------------------
class ExibeTweets(TemplateView):

    template_name = "exibe_tweets.html"

    def get_context_data(self, **kwargs):
        context = super(ExibeTweets, self).get_context_data(**kwargs)
        api = slumber.API(conf.URL_API)
        context.update({'tweets': api.tweets.get()})
        context.update({'usuarios': api.users.get()})

        return context
