from django.conf.urls import patterns, include, url
from django.contrib import admin

import twitterapp.views

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', twitterapp.views.ExibeTweets.as_view(), name="exibe_tweets",), 
    url(r'^usuarios/', twitterapp.views.ExibeUsuarios.as_view(), name="exibe_usuarios",), 
    url(r'^novousuario/', twitterapp.views.CadastraUsuarios.as_view(), name="cadastra_usuarios.html"),
    url(r'^novotweet/', twitterapp.views.EnviaTweet.as_view(), name="envia_tweet.html",), 
    url(r'^login/', twitterapp.views.LoginView.as_view(), name="login.html",), 
    url(r'^home/', twitterapp.views.HomeView.as_view(), name="index.html",), 


)
